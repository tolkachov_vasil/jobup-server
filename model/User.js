import DataLoader from 'dataloader'
import findByIds from 'mongo-find-by-ids'
import bcrypt from 'bcrypt'
import {ObjectId} from 'mongodb'

const SALT_ROUNDS = 10

export default class User {
  constructor(context) {
    this.context = context
    this.collection = context.db.collection('user')
    this.pubsub = context.pubsub
    this.loader = new DataLoader(ids => findByIds(this.collection, ids))
  }

  findOneById(id) {
    return this.loader.load(ObjectId(id))
  }

  all({lastCreatedAt = 0, limit = 10}) {
    return this.collection.find({
      createdAt: {$gt: lastCreatedAt},
    }).sort({createdAt: 1}).limit(limit).toArray()
  }

  id(user) {
    return this.context.ObjID.findOneById(user.idId)
  }

  async insert(doc) {
    // We don't want to store passwords plaintext!
    const {password, ...rest} = doc
    const hash = await bcrypt.hash(password, SALT_ROUNDS)
    const docToInsert = Object.assign({}, rest, {
      hash,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    })
    const id = (await this.collection.insertOne(docToInsert)).insertedId
    this.pubsub.publish('userInserted', await this.findOneById(id))
    return id
  }

  async updateById(id, doc) {
    const ret = await this.collection.updateOne({_id: id}, {
      $set: Object.assign({}, doc, {
        updatedAt: Date.now(),
      }),
    })
    this.loader.clear(id)
    this.pubsub.publish('userUpdated', await this.findOneById(id))
    return ret
  }

  async removeById(id) {
    const ret = this.collection.deleteOne({_id: id})
    this.loader.clear(id)
    this.pubsub.publish('userRemoved', id)
    return ret
  }
}
