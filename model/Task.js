import DataLoader from 'dataloader'
import findByIds from 'mongo-find-by-ids'
import {ObjectId} from 'mongodb'

export default class Task {
  constructor(context) {
    this.context = context
    this.collection = context.db.collection('task')
    this.pubsub = context.pubsub
    this.loader = new DataLoader(ids => findByIds(this.collection, ids))
  }

  findOneById(id) {
    return this.loader.load(ObjectId(id))
  }

  all({lastCreatedAt = 0, limit = 10}) {
    return this.collection.find({
      createdAt: {$gt: lastCreatedAt},
    }).sort({createdAt: 1}).limit(limit).toArray()
  }

  id(task) {
    return this.context.ObjID.findOneById(task.idId)
  }

  user(task) {
    return this.context.User.findOneById(task.userId)
  }

  async insert(doc) {
    const docToInsert = Object.assign({}, doc, {
      createdAt: Date.now(),
      updatedAt: Date.now(),
    })
    const id = (await this.collection.insertOne(docToInsert)).insertedId
    this.pubsub.publish('taskInserted', await this.findOneById(id))
    return id
  }

  async updateById(id, doc) {
    const ret = await this.collection.updateOne({_id: ObjectId(id)}, {
      $set: Object.assign({}, doc, {
        updatedAt: Date.now(),
      }),
    })
    this.loader.clear(id)
    this.pubsub.publish('taskUpdated', await this.findOneById(id))
    return ret
  }

  async removeById(id) {
    const ret = this.collection.deleteOne({_id: ObjectId(id)})
    this.loader.clear(id)
    this.pubsub.publish('taskRemoved', id)
    return ret
  }
}
